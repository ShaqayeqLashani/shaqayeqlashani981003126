import {RouterModule,Routes} from "@angular/router";
import {UserComponent} from "./user/user.component";
import {ChatComponent} from "./chat/chat.component";
import {ManagerComponent} from "./manager/manager.component";
import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
const routes: Routes = [
  {
    path:'user',
    component:UserComponent,
    pathMatch:'full'
  },
  {
    path:'chat',
    component:ChatComponent,
    pathMatch:'full'
  },
  {
    path:'manager',
    component:ManagerComponent,
    pathMatch:'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ],
  declarations:[]
})
export class AppRoutingModule { }
