import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {until} from "selenium-webdriver";
import urlIs = until.urlIs;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shqyq';

  constructor(private router: Router) {
  }
}
